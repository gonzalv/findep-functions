const ENDPOINTS = {
  PERSON: {
      REGISTER: "https://findep-desarrollo-170215.appspot.com/CrediwebProxy/cxf/personasFisa/PersonaService/registrarPersona",
      UPDATE: "https://findep-desarrollo-170215.appspot.com/CrediwebProxy/cxf/personasFisa/PersonaService/actualizarPersona",
      QUERY: "https://findep-desarrollo-170215.appspot.com/CrediwebProxy/cxf/personasFisa/PersonaService/consultarPersona",
  },
};

module.exports.BUCKET_NAME = 'test-functions-2044a.appspot.com';
module.exports.ENDPOINTS = ENDPOINTS;
