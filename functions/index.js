const functions = require('firebase-functions');

// Constants
const {
    BUCKET_NAME,
} = require('./constants');

// Module Functions

const pdfGeneratorFunctionFunction =
    require('./pdfGenerator/pdfGenerator');
const campaignStatusFunction =
    require('./campaing/getStatus');
const operativeNetworkFunction =
    require('./operativeNetwork/getInfo');
const reasignationAppointmentsFunction =
    require('./appointments/reasignation');
const reasignationRenewalsFunction =
    require('./renewals/reasignation');

/*
const registerPersonFunction = require('./person/register');
const updatePersonFunction = require('./person/update');
const queryPersonFunction = require('./person/query');
const generateRequestFunction = require('./request/generate');
const resizeImageFunction = require('./utils/resizeImage');
const moveImagesFunction = require('./utils/moveImages');
const makeRequestFunction = require('./utils/makeRequest');
const addImagePdfFunction = require('./utils/addImagePdf');
*/

// Functions

/*
// Query person
exports.users = functions
    .https
    .onRequest(queryPersonFunction);

// Register Person
exports.registerPerson = functions
    .database
    .ref('clients/{assessorId}/news/{firebaseId}')
    .onCreate(registerPersonFunction);

// Update Person
exports.updatePerson = functions
    .database
    .ref('/clients/{assessorId}/update/{personId}')
    .onCreate(updatePersonFunction);

// Resize Images
exports.imageResizer = functions
    .storage
    .bucket(BUCKET_NAME)
    .object()
    .onFinalize(resizeImageFunction);

// Move images .jpg and .png to images/{id}
exports.moveImages = functions
    .storage
    .bucket(BUCKET_NAME)
    .object()
    .onFinalize(moveImagesFunction);

// Make Request
exports.makeRequest = functions
    .database
    .ref('/requests/{firebaseId}')
    .onCreate(makeRequestFunction);
*/

// Resignation to appointments
exports.reasignationAppointments = functions
    .https
    .onRequest(reasignationAppointmentsFunction);

// Resignation to appointments
exports.reasignationRenewals = functions
    .https
    .onRequest(reasignationRenewalsFunction);


// PDF Generator function.
exports.pdfGenerator = functions
    .https
    //.ref('pdf_generator/{firebaseId}')
    .onRequest(pdfGeneratorFunctionFunction);

// Get campaign status function
exports.campaignStatus = functions
    .https
    .onRequest(campaignStatusFunction);

exports.operativeNetwork = functions
    .runWith({memory: "1GB"})
    .storage
    .bucket(BUCKET_NAME)
    .object()
    .onFinalize(operativeNetworkFunction);