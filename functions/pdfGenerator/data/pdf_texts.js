module.exports = (data) => ([
    // Page 1.
    [{
        "size": 12,
        "text": data.nombre,
        "x": 250,
        "y": 152,
    }],
    // Page 2.
    [],
    // Page 3.
    [{
        "size": 12,
        "text": data.tasa_interes,
        "x": 520,
        "y": 408,
    }],
    // Page 4.
    [],
    // Page 5.
    [],
    // Page 6.
    [],
    // Page 7.
    [
        {
            "size": 12,
            "text": data.dia,
            "x": 71,
            "y": 177,
        },
        {
            "size": 12,
            "text": data.mes,
            "x": 190,
            "y": 177,
        },
        {
            "size": 12,
            "text": data.anio,
            "x": 302,
            "y": 177,
        },
        {
            "size": 12,
            "text": data.nombre,
            "x": 410,
            "y": 310,
        },
    ],
    // Page 8.
    [
        {
            "size": 12,
            "text": data.ciudad,
            "x": 70,
            "y": 182,
        },
        {
            "size": 12,
            "text": data.estado,
            "x": 200,
            "y": 182,
        },
        {
            "size": 12,
            "text": data.dia,
            "x": 323,
            "y": 182,
        },
        {
            "size": 12,
            "text": data.mes,
            "x": 422,
            "y": 182,
        },
        {
            "size": 12,
            "text": data.anio,
            "x": 545,
            "y": 182,
        },
        {
            "size": 12,
            "text": data.nombre,
            "x": 242,
            "y": 650,
        },
    ],
    // Page 9.
    [
        {
            "size": 12,
            "text": data.nombre,
            "x": 100,
            "y": 110,
        },
        {
            "size": 12,
            "text": `${data.dia}/${data.mes}/${data.anio}`,
            "x": 470,
            "y": 113
        },
        {
            "size": 12,
            "text": data.cat,
            "x": 90,
            "y": 190
        },
        {
            "size": 12,
            "text": data.tasa_interes,
            "x": 210,
            "y": 190
        },
        {
            "size": 12,
            "text": data.monto_credito,
            "x": 330,
            "y": 190,
        },
        {
            "size": 12,
            "text": data.monto_pagar,
            "x": 470,
            "y": 190
        },
        {
            "size": 12,
            "text": data.calculo_interes,
            "x": 210,
            "y": 270,
        },
        {
            "size": 12,
            "text": data.monto_pago,
            "x": 330,
            "y": 275,
        },
        {
            "size": 12,
            "text": data.nombre,
            "x": 355,
            "y": 720,
        },
        {
            "size": 12,
            "text": data.plazo_credito,
            "x": 60,
            "y": 313,
        },
        {
            "size": 11,
            "text": data.fecha_limite,
            "x": 370,
            "y": 297,
        },
        {
            "size": 12,
            "text": data.fecha_corte,
            "x": 340,
            "y": 315,
        }
    ],
    // Page 10.
    [
        {
            "size": 12,
            "text": data.nombre,
            "x": 207,
            "y": 143,
        },
        {
            "size": 12,
            "text": data.caracter_empleado,
            "x": 150,
            "y": 195,
        },
        {
            "size": 12,
            "text": data.cantidad_numero,
            "x": 150,
            "y": 300,
        },
        {
            "size": 12,
            "text": data.cantidad_letra,
            "x": 150,
            "y": 325,
        },
        {
            "size": 12,
            "text": data.manera,
            "x": 110,
            "y": 342,
        },
        {
            "size": 12,
            "text": data.periodo,
            "x": 330,
            "y": 342,
        },
        {
            "size": 12,
            "text": data.id_solicitud,
            "x": 140,
            "y": 392,
        },
        {
            "size": 12,
            "text": data.dia,
            "x": 330,
            "y": 392,
        },
        {
            "size": 12,
            "text": data.mes,
            "x": 420,
            "y": 392,
        },
        {
            "size": 12,
            "text": data.anio,
            "x": 530,
            "y": 392,
        },
        {
            "size": 12,
            "text": data.dia,
            "x": 180,
            "y": 440,
        },
        {
            "size": 12,
            "text": data.mes,
            "x": 290,
            "y": 440,
        },
        {
            "size": 12,
            "text": data.anio,
            "x": 400,
            "y": 440,
        },
        {
            "size": 12,
            "text": data.nombre,
            "x": 300,
            "y": 557,
        },
        {
            "size": 12,
            "text": data.dato_1,
            "x": 40,
            "y": 665,
        },
        {
            "size": 12,
            "text": data.dato_2,
            "x": 140,
            "y": 665,
        },
        {
            "size": 12,
            "text": data.dato_3,
            "x": 255,
            "y": 665,
        },
        {
            "size": 12,
            "text": data.dato_4,
            "x": 400,
            "y": 665,
        },
        {
            "size": 12,
            "text": data.dato_5,
            "x": 525,
            "y": 665,
        }
    ],
    // Page 11.
    [
        {
            "size": 12,
            "text": data.ciudad,
            "x": 55,
            "y": 148,
        },
        {
            "size": 12,
            "text": data.estado,
            "x": 230,
            "y": 148,
        },
        {
            "size": 12,
            "text": data.dia,
            "x": 358,
            "y": 148,
        },
        {
            "size": 12,
            "text": data.mes,
            "x": 440,
            "y": 148,
        },
        {
            "size": 12,
            "text": data.anio,
            "x": 540,
            "y": 148,
        },
        {
            "size": 12,
            "text": data.nombre,
            "x": 130,
            "y": 225,
        },
        {
            "size": 12,
            "text": data.id_numero,
            "x": 100,
            "y": 262,
        },
        {
            "size": 12,
            "text": `${data.dia}/${data.mes}/${data.anio}`,
            "x": 300,
            "y": 262,
        },
        {
            "size": 12,
            "text": data.cantidad_numero,
            "x": 180,
            "y": 302,
        },
        {
            "size": 12,
            "text": data.cantidad_letra,
            "x": 180,
            "y": 322,
        },
        {
            "size": 12,
            "text": data.cantidad_numero,
            "x": 50,
            "y": 404,
        },
        {
            "size": 12,
            "text": data.cantidad_letra,
            "x": 250,
            "y": 404,
        },
        {
            "size": 12,
            "text": data.titular,
            "x": 150,
            "y": 460,
        },
        {
            "size": 12,
            "text": data.clabe,
            "x": 150,
            "y": 496,
        },
        {
            "size": 12,
            "text": data.cuenta,
            "x": 150,
            "y": 528,
        },
        {
            "size": 12,
            "text": data.banco,
            "x": 150,
            "y": 560,
        }
    ],
    // Page 12.
    [
        {
            "size": 10,
            "text": data.nombre,
            "x": 200,
            "y": 104,
        },
        {
            "size": 10,
            "text": data.cantidad_numero,
            "x": 400,
            "y": 136,
        },
        {
            "size": 10,
            "text": data.cantidad_letra,
            "x": 50,
            "y": 152,
        },
        {
            "size": 8,
            "text": data.liq,
            "x": 464,
            "y": 164,
        },
        {
            "size": 8,
            "text": data.iva,
            "x": 444,
            "y": 306,
        },
        {
            "size": 10,
            "text": data.ciudad,
            "x": 410,
            "y": 500,
        },
        {
            "size": 10,
            "text": data.estado,
            "x": 70,
            "y": 512,
        },
        {
            "size": 8,
            "text": data.nombre,
            "x": 250,
            "y": 554,
        },
        {
            "size": 10,
            "text": data.ciudad,
            "x": 110,
            "y": 582,
        },
        {
            "size": 10,
            "text": data.estado,
            "x": 285,
            "y": 582,
        },
        {
            "size": 10,
            "text": data.dia,
            "x": 493,
            "y": 582,
        },
        {
            "size": 10,
            "text": data.mes,
            "x": 70,
            "y": 596,
        },
        {
            "size": 10,
            "text": data.anio,
            "x": 270,
            "y": 596,
        }
    ]
]);