module.exports = sign_path => ({
    "path": sign_path,
    "7": {x: 425, y: 220, fit: 90},
    "8": {x: 260, y: 560, fit: 100},
    "9": {x: 370, y: 630, fit: 100},
    "11": {x: 255, y: 650, fit: 70},
    "12": {x: 150, y: 630, fit: 50},
});