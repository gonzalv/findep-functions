const PdfDocument = require('pdfkit');
const uuidV4 = require('uuid/v4');

const downloadFile = require('../utils/downloadFile');
const deleteFile = require('../utils/deleteFile');
const getPdfData = require('./getPdfData');
const {bucket} = require('../admin');

module.exports = (req, res) => {
    console.log("Version 2.0: ", req.body);

    console.log("Downloading images...");
    let images = [];
    for (let i = 1; i <= 12; i++) {
        const path = downloadFile(`pdfImages/${i}.png`);
        images.push(path);
    }

    return Promise.all(images)
        .then(async (imagePathList) => {
            console.log("All images downloaded.");
            const doc = new PdfDocument({
                autoFirstPage: false,
                compress: false
            });
            const pdfName = `${uuidV4()}.pdf`;
            const pdfFile = bucket.file(`pdf/${pdfName}`);
            const pdfStream = pdfFile.createWriteStream({
                resumable: false
            });

            const {dataSigns, dataTexts} = getPdfData(req.body);

            const signPath = await downloadFile(dataSigns["path"]);

            const signIndex = new Set([6, 7, 8, 10, 11]);
            imagePathList.map((imagePath, n) => {
                doc.addPage();
                doc.image(imagePath, 0, 0, {width: 610});
                if (signIndex.has(n)) {
                    const dataSign = dataSigns[(n + 1).toString()];
                    doc.image(signPath, dataSign.x, dataSign.y, {
                        fit: [dataSign.fit, dataSign.fit]
                    });
                }

                const dataText = dataTexts[n];
                dataText.map(elem => {
                    doc.fontSize(elem.size).text(elem.text, elem.x, elem.y, {lineBreak: false});
                    console.log(elem);
                });
            });

            doc.pipe(pdfStream);
            doc.end();

            pdfStream.addListener('finish', () => {
                deleteFile(signPath);
                console.log("Deleting images...");
                imagePathList.map(imagePath =>
                    deleteFile(imagePath)
                );
                console.log("PDF uploaded to: ", pdfFile.name);
                return res.send({pdfPath: pdfFile.name});

                /*
                snap.ref.update({pdfPath: pdfFile.name})
                    .then(() =>
                        console.log("PDF uploaded to: ", pdfFile.name)
                    );
                    */
            });
        });
};