
// Info que se necesita
// --- HOJA 1
// - Nombre completo del acreditado [nombre]
// - Path a la firma en storage [sign_path]

// --- HOJA 3
// - Monto de la tasa de interes [tasa_interes]

// --- HOJA 7
// - Dia [dia]
// - Mes [mes]
// - Año [anio]

// --- HOJA 8
// - Ciudad [ciudad]
// - Estado [estado]

// --- HOJA  9
// - CAT [cat]
// - Monto crédito [monto_credito]
// - Monto pagar [monto_pagar]
// - Calculo de intereses [calculo_interes] (Aquí manda global o insoluto)
// - Monto pago [monto_pago]
// - Plazo del crédito [plazo_credito]
// - Fecha límite del pago [fecha_limite]
// - Fecha de corte [fecha_corte]

// --- HOJA 10
// - Caracter de empleado [caracter empleado] (Revisa el pdf xD)
// - Cantidad numero [cantidad_numero] (Revisa el pdf xD)
// - Cantidad letra [cantidad_letra] (Revisa el pdf xD)
// - Manera [manera] (Revisa el pdf xD)
// - Periodo [periodo] (Revisa el pdf xD)
// - Id numero solicitud [id_solicitud]
// - Datos 1 - 5 [dato_n]

// --- HOJA 11
// - Id  con numero [id_numero]
// - Titular [titular]
// - CLABE [clabe]
// - Cuenta [cuenta]
// - Banco [banco]

// --- HOJA 12
// - LIQ [liq]
// - IVA [iva]


const dataSigns = require('./data/pdf_signs');
const dataTexts = require('./data/pdf_texts');

module.exports = data => {
    console.log("Getting data: ", data);
    return {
        dataSigns: dataSigns(data.sign_path),
        dataTexts: dataTexts(data),
    };
};