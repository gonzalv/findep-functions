// Dependencies
const fs = require('fs');

module.exports = filePath => {
    fs.unlink(filePath, () =>
        console.log("File deleted from: ", filePath));
};