const axios = require('axios');
const {ENDPOINTS} = require('./../constants');

module.exports = async (snap) => {
    const message = snap.val();
    const response = await axios(buildRequest(message));
    return snap.ref.update({new_response: response})
        .then(() => console.log("Response: ", response));
};

const buildRequest = message => ({
    url: ENDPOINTS.PERSON.UPDATE,
    method: 'put',
    headers: {
        "Content-Type": "application/json",
    },
    data: message,
});