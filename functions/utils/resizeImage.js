const sharp = require('sharp');
const gcs = require('@google-cloud/storage')();
const path = require('path');
const os = require('os');
const fs = require('fs');

module.exports = async (obj) => {
    try {
        const filePath = await obj.name;
        console.log("filePath: ", filePath);
        if (!filePath.includes('images')) {
            console.log("I can handle this file: ", filePath);
            return;
        }

        const fileName = path.basename(filePath);
        if (fileName.includes('thumb') || fileName.includes('medium')) {
            console.log("Already a processed image.");
            return;
        }

        console.log("Creating thumbnail image...");
        await createProcessedImage(obj, {
            size: 350,
            type: "thumb"
        });

        console.log("Creating medium image...");
        await createProcessedImage(obj, {
            size: 1000,
            type: "medium"
        });

    } catch (error) {
        console.log("Error on function: ", error);
    }
};

const createProcessedImage = async (obj, options) => {
    const filePath = obj.name;
    const fileName = path.basename(filePath);

    const fileBucket = obj.bucket;
    const contentType = obj.contentType;
    const bucket = gcs.bucket(fileBucket);
    const tempFilePath = path.join(os.tmpdir(), fileName);
    const processedFilePath = path.join(path.dirname(tempFilePath), `processed_${fileName}`);

    const metadata = {
        contentType: contentType,
    };
    return bucket.file(filePath).download({
        destination: tempFilePath,
    }).then(() => {
        console.log('Image downloaded locally to', tempFilePath);

        // return spawn('convert', [tempFilePath, '-thumbnail', `${options.size}>`, tempFilePath]);
        return sharp(tempFilePath)
            .resize(options.size)
            .toFile(processedFilePath);
    }).then(() => {
        console.log('Processed image created at', processedFilePath);

        const extension = path.extname(filePath);
        const fileNameExt = path.basename(filePath, extension);
        const processedFileName = `${fileNameExt}.${options.type}.${extension}`;
        const newPathToBucket = path.join(path.dirname(filePath), processedFileName);

        return bucket.upload(processedFilePath, {
            destination: newPathToBucket,
            metadata: metadata,
        });

    }).then(() => {
        fs.unlinkSync(tempFilePath);
        fs.unlinkSync(processedFilePath);
    });
};