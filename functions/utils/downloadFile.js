// Dependencies
const os = require('os');
const path = require('path');

// Modules
const {bucket} = require('./../admin');

// Function
module.exports = async filePath => {
    const fileName = path.basename(filePath);
    const tempFilePath = `${os.tmpdir()}/${fileName}`;

    await bucket.file(filePath).download({
        destination: tempFilePath
    });
    console.log("Image downloaded to: ", tempFilePath);
    return tempFilePath;
};