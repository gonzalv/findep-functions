const gcs = require('@google-cloud/storage')();
const path = require('path');
const os = require('os');
const fs = require('fs');


module.exports = async (obj) => {
    const filePath = obj.name;

    if (!filePath.includes('.jpg') && !filePath.includes('.png')) {
        console.log("I can't handle this file.");
        return;
    }

    if (filePath.includes('images')) {
        console.log("Image already in ", filePath);
        return;
    }

    const fileBucket = obj.bucket;
    const fileName = path.basename(filePath);
    const bucket = gcs.bucket(fileBucket);
    const tempFilePath = path.join(os.tmpdir(), fileName);
    const metadata = {
        contentType: obj.contentType,
    };

    const id = path.basename(path.dirname(filePath));
    const destinationPath = path.join('images', id, fileName);

    return bucket.file(filePath).download({
        destination: tempFilePath,
    }).then(() => {
        console.log("Image downloaded to ", tempFilePath);
        return bucket.upload(tempFilePath, {
            destination: destinationPath,
            metadata: metadata,
        })
    }).then(() => {
        console.log("Image uploaded to ", destinationPath);
        fs.unlinkSync(tempFilePath);
        console.log("Image deleted from tempFile ", tempFilePath);
    });
};