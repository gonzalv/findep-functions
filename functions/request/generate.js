const {admin} = require('./../admin');

module.exports = (snap, context) => {
    try {
        const pushId = snap.ref.key;
        console.log(`pushId: ${pushId}`);

        // TODO: Make the request to findep services.
        const folio = "test-foil";
        console.log(folio);

        const dataRequest = snap.val();
        dataRequest["folio"] = folio;
        dataRequest["idFirebase"] = pushId;

        admin.database().ref("/requests").child(folio).set(dataRequest);
        console.log(`Request moved to branch 'requests' with folio:${folio} successful`);

        return snap.ref.remove()
    } catch (err) {
        console.log("requestGenerator error");
        console.log(err)
    }
};