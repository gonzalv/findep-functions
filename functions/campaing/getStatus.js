const axios = require('axios');
const {admin} = require('./../admin');

module.exports = async (req, res) => {
    try {
        //const date = new Date();
        //const finalDate = formatDate(date);
        const finalDate = req.body.finalDate;
        console.log("Final date: ", finalDate);

        //date.setMonth(date.getMonth() - 1);
        //const startDate = formatDate(date);
        const startDate = req.body.startDate;
        console.log("Start date: ", startDate);

        const sellers = req.body.sellers;
        console.log("Sellers: ", sellers);

        const message = {
            "oficina": {
                "oficina": 144
            },
            "status": "P",
            "fechaInicio": startDate,
            "fechaFin": finalDate,
            "empresa": "000100000000",
            "segmento": "FORM",
            "categoria": "NOMN",
            "tipoProducto": "TRAD",
            "usuarioRegistraSolicitud": "000724385111",
            "vendedores": sellers
        };

        console.log("Making the request to FinDep");
        const findepRes = await axios(buildRequest(message));

        const payload = findepRes.data.payload;
        console.log(payload);

        const data = processData(payload);
        admin.database().ref('/control').remove();
        for(let sellerId in data) {
            admin.database().ref(`/control/${sellerId}`).update(JSON.parse(JSON.stringify(data[sellerId])));
        }

        return res.send(findepRes.data);
    } catch (error) {
        console.log("Error on: ", error);
        res.send(error);
    }
};

const buildRequest = (message) => ({
    url: "https://crediwebproxy-dot-findep-desarrollo-170215.appspot.com/CrediwebProxy/Solicitud/consultarSolicitudesXoficinaPrincipal",
    method: 'post',
    headers: {
        "Content-Type": "application/json",
    },
    data: message
});

const processData = data => {
    const result = data.reduce((data, request) => {
        let seller = request["vendedor"];
        data[seller] = (data[seller] === undefined)? [request] : data[seller].concat([request]);
        return data;
    }, {});
    return result;
};

/*
const formatDate = (date) => {
    let month = date.getMonth() + 1;
    if (month.length === 1) {
        month = "0" + month;
    }
    const year = date.getFullYear();
    let day = date.getDate();
    if (day.length === 1) {
        day = "0" + day;
    }
    return `${day}/${month}/${year}`;
};
*/