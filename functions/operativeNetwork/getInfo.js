const mkdirp = require('mkdirp-promise');
const gcs = require('@google-cloud/storage')();
const path = require('path');
const os = require('os');
const fs = require('fs');
const XLSX = require('xlsx');
const camelCase = require('camelcase');
const {admin} = require('./../admin');

module.exports = async (object) => {
    console.log('Loading object: ', object);
    let filePath = object.name;

    if (!filePath.includes('semana-empleados')) {
        console.log('I can´t handle this file');
        return;
    }

    const version = "1.0";
    console.log(`***** Version ${version} ******`);

    const tmpPath = filePath + new Date().getTime().toString();
    console.log(tmpPath);
    const tempLocalFile = path.join(os.tmpdir(), tmpPath);
    const tempLocalDir = path.dirname(tempLocalFile);

    const bucket = gcs.bucket(object.bucket);
    return mkdirp(tempLocalDir)
        .then(() => {
            console.log('Downloading: ', filePath);
            return bucket.file(filePath).download({destination: tempLocalFile})
        }).then(async () => {
            console.log('The file has been downloaded to: ', tempLocalFile);
            let workbook = XLSX.readFile(tempLocalFile);
            const sheetName = workbook.SheetNames[0];
            let worksheet = workbook.Sheets[sheetName];

            const cells = genCharArray('A', 'S');
            let headers = cells.map(character => {
                let cell = character + '1';
                return camelCase(worksheet[cell].v)
            });
            console.log('Headers: ', headers);

            let jsonWorkbook = XLSX.utils.sheet_to_json(worksheet, {header: headers});
            jsonWorkbook.pop();
            const data = processData(jsonWorkbook);
            console.log('Data: ', data);
            let employersPath = '/operative-network';
            await admin.database().ref(employersPath).remove()
                .then(() => console.log('Deleted: ', employersPath));
            for (let divisionId in data) {
                admin.database()
                    .ref(`${employersPath}/${divisionId}`)
                    .update(JSON.parse(JSON.stringify(data[divisionId])));
            }
        }).then(() => {
            console.log('Deleting file from: ', tempLocalFile);
            fs.unlinkSync(tempLocalFile)
        });
};

const genCharArray = (charA, charZ) => {
    let a = [], i = charA.charCodeAt(0), j = charZ.charCodeAt(0);
    for (; i <= j; ++i) {
        a.push(String.fromCharCode(i));
    }
    return a;
};

const processData = arr => {
    return arr.reduce((data, elem) => {
        if (elem["noPersona"] === "No Persona") return data;

        let numDiv = elem["numDiv"];
        if (!numDiv) numDiv = 0;
        let numZonal = elem["numZonal"];
        if (!numZonal) numZonal = 0;
        let numSup = elem["numSup"];
        if (!numSup) numSup = 0;
        let noPersona = elem["noPersona"];
        if (!noPersona) noPersona = 0;

        if (data[numDiv] === undefined) {
            data[numDiv] = {[numZonal]: {[numSup]: {[noPersona]: elem}}};
            return data;
        }
        if (data[numDiv][numZonal] === undefined) {
            data[numDiv][numZonal] = {[numSup]: {[noPersona]: elem}};
            return data;
        }

        data[numDiv] = {
            ...data[numDiv],
            [numZonal]: {
                ...data[numDiv][numZonal],
                [numSup]: {
                    ...data[numDiv][numZonal][numSup],
                    [noPersona]: elem
                }
            }
        };

        return data;
    }, {});
};