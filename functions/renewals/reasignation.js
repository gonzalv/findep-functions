const {admin} = require('./../admin');
const path = require('path');

module.exports = async (req, res) => {
    const data = req.body;
    const oldId = data.oldId.toString();
    const newId = data.newId.toString();
    const renewalId = data.renewalId.toString();
    console.log("Body request: ", data);

    const oldRenewalRef = await admin
        .database()
        .ref(path.join("admin/renewals", oldId, renewalId));
    console.log("oldAppointmentRef: ", oldRenewalRef.toJSON());

    const oldRenewalData = await oldRenewalRef
        .once('value')
        .then((snap) => snap);
    console.log("oldRenewalData ", oldRenewalData);

    const newRenewalRef = await admin
        .database()
        .ref(path.join("admin/renewals", newId, renewalId));
    console.log("newRenewalRef: ", newRenewalRef.toJSON());

    return newRenewalRef
        .set(oldRenewalData.val())
        .then(() => {
            console.log("Setting oldRenewalData");
            return oldRenewalRef.parent.remove();
        })
        .then(() => {
            console.log("Deleting oldRenewalRef");
            return res.send(data);
        });
};