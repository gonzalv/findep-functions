const axios = require('axios');
const {ENDPOINTS} = require('./../constants');

module.exports = async (req, res) => {
    try {
        console.log("Loading...");
        let idPerson = req.query.idPersona;
        console.log(idPerson);
        let message = {idPersona: idPerson};

        console.log("Making the request to FinDep");
        const findepRes = await axios(buildRequest(message));

        console.log(findepRes.data);

        res.send(JSON.stringify(findepRes.data.payload));
    } catch (error) {
        console.log('Error on request');
        console.log(error);
        res.send(error);
    }
};

const buildRequest = (message) => ({
    url: ENDPOINTS.PERSON.QUERY,
    method: 'post',
    headers: {
        "Content-Type": "application/json",
    },
    data: message
});