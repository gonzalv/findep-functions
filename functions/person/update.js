const {admin} = require('./../admin');
const axios = require('axios');
const {ENDPOINTS} = require('./../constants');

module.exports = async (snap) => {
    const idPerson = snap.key;
    const assessorId = snap.ref.parent.parent.key;
    const assessorPortfolioRef = admin
        .database()
        .ref(`/clients/${assessorId}/portfolio`);

    // Make request to findep services.
    const message = snap.val();
    console.log(message);

    console.log("Making request.");
    const findepRes = await axios(buildRequest(message));
    console.log(findepRes);
    const payload = findepRes.data.payload;

    assessorPortfolioRef.update({[idPerson]: payload})
        .then(() =>
            console.log("Writing: ", payload)
        );
    snap.ref.remove().then(() =>
        console.log("Cleaning ", snap.key)
    );
};

const buildRequest = message => ({
    url: ENDPOINTS.PERSON.UPDATE,
    method: 'put',
    headers: {
        "Content-Type": "application/json",
    },
    data: message,
});