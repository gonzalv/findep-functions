const {admin} = require('./../admin');
const {ENDPOINTS} = require('./../constants');
const axios = require('axios');

module.exports = async (snap) => {
    const assessorId = snap.ref.parent.parent.key;
    const assessorPortfolioRef = admin
        .database()
        .ref(`/clients/${assessorId}/portfolio`);

    // Make request to findep services
    const message = snap.val();
    const findepRes = await axios(buildRequest(message));
    const payload = findepRes.data.payload;

    assessorPortfolioRef.update({[payload.idPersona]: payload})
        .then(() =>
            console.log("Writing: ", payload)
        );
    snap.ref.remove().then(() =>
        console.log("Cleaning ", snap.key)
    );
};

const buildRequest = message => ({
    url: ENDPOINTS.PERSON.REGISTER,
    method: 'post',
    headers: {
        "Content-Type": "application/json",
    },
    data: message,
});