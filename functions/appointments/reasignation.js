const {admin} = require('./../admin');
const path = require('path');

module.exports = async (req, res) => {
    const data = req.body;
    const oldId = data.oldId.toString();
    const newId = data.newId.toString();
    const appointmentId = data.appointmentId.toString();
    console.log("Body request: ", data);

    const oldAppointmentRef = await admin
        .database()
        .ref(path.join("admin/appointments", oldId, appointmentId));
    console.log("oldAppointmentRef: ", oldAppointmentRef.toJSON());

    const oldAppointmentData = await oldAppointmentRef
        .once('value')
        .then((snap) => snap);
    console.log("oldAppointmentData ", oldAppointmentData);

    const newAppointmentRef = await admin
        .database()
        .ref(path.join("admin/appointments", newId, appointmentId));
    console.log("newAppointmentRef: ", newAppointmentRef.toJSON());

    return newAppointmentRef
        .set(oldAppointmentData.val())
        .then(() => {
            console.log("Setting oldAppointmentData");
            return oldAppointmentRef.parent.remove();
        })
        .then(() => {
            console.log("Deleting oldAppointmentRef");
            return res.send(data);
        });
};