const admin = require('firebase-admin');
const gcs = require('@google-cloud/storage')();
const {BUCKET_NAME} = require('./constants');

const serviceAccount = require('./project-credentials');
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://test-functions-2044a.firebaseio.com/",
});

module.exports.admin = admin;

module.exports.bucket = gcs.bucket(BUCKET_NAME);